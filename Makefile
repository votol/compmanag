all: 
	mkdir -p ./build
	mkdir -p ./libs
	mkdir -p ./build/libs
	cmake -D CMAKE_BUILD_TYPE=Release -B./build/libs -H./code/libs
	make -C ./build/libs -j `nproc` VERBOSE=1
tests: ./code/tests
	mkdir -p ./tests
	mkdir -p ./build/tests
	cmake -D CMAKE_BUILD_TYPE=Release -B./build/tests -H./code/tests
	make -C ./build/tests -j `nproc` VERBOSE=1
	
	./tests/CompManagTests
	
