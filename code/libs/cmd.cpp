#include <iostream>
#include "CompManag_cmd.hpp"



using namespace CompManag;

cmd_processor::IntVariable::IntVariable(int* ptr):my_ptr(ptr)
    {
    }

void cmd_processor::IntVariable::SetValue(const std::string& in)
    {
    *my_ptr=std::stoi(in);
    }

void cmd_processor::IntVariable::PrintValue()
    {
    std::cout<<*my_ptr;
    }

cmd_processor::DoubleVariable::DoubleVariable(double* ptr):my_ptr(ptr)
    {
    }

void cmd_processor::DoubleVariable::SetValue(const std::string& in)
    {
    *my_ptr=std::stod(in);
    }

void cmd_processor::DoubleVariable::PrintValue()
    {
    std::cout<<*my_ptr;
    }

cmd_processor::IParameterVariable::IParameterVariable(std::shared_ptr<IParameter> ptr):my_ptr(ptr)
    {
    }

void cmd_processor::IParameterVariable::SetValue(const std::string& in)
    {
    my_ptr->SetValue(std::stod(in));
    }

void cmd_processor::IParameterVariable::PrintValue()
    {
    std::cout<<my_ptr->GetValue();
    }

void cmd_processor::print(const std::list<std::string>& params)
    {
    for(auto it=variables.begin();it!=variables.end();++it)
        {
        std::cout<<it->first<<" = ";
        it->second->PrintValue();
        std::cout<<std::endl;
        }
    }

void cmd_processor::exit(const std::list<std::string>& params)
    {
    IsFinished=true;
    }

void cmd_processor::help(const std::list<std::string>& params)
    {
    for(auto it=commands.begin();it!=commands.end();++it)
        {
        std::cout<<it->first<<std::endl;
        }
    }

void cmd_processor::variable_processor(std::string& name,std::string& val)
    {
    std::string num_val;
    bool printing;
    if(variables.find(name)==variables.end())
        {
        std::cout<<"Unknown variable: "<<name<<std::endl;
        return;
        }
    printing=true;
    for(auto it=val.begin();it!=val.end();++it)
        {
        if(*it==';')
            {
            printing=false;
            break;
            }
        else if(*it!=' ')
            num_val.push_back(*it);
        }
    variables[name]->SetValue(num_val);
    if(printing==true)
        print(std::list<std::string>());
    }

void cmd_processor::function_processor(std::string& name,std::string& val)
    {
    if(commands.find(name)==commands.end())
        {
        std::cout<<"Unknown command: "<<name<<std::endl;
        return;
        }
    std::list<std::string> tmp_params_list;
    tmp_params_list.push_back(std::string(""));
    if(val.size()==0)
        {
        commands[name](tmp_params_list);
        return;
        }
    
    int braket_count=1;
    auto it=val.begin();
    ++it;
    
    while(1)
        {
        if(it==val.end())
            {
            std::cout<<"Wrong brakets\n";
            return;
            }
        if(*it==')')
            {
            braket_count--;
            if(braket_count==0)
                break;
            tmp_params_list.back().push_back(*it);
            }
        else if(*it=='(')
            {
            braket_count++;
            tmp_params_list.back().push_back(*it);
            }
        else if(*it==',')
            {
            tmp_params_list.push_back(std::string(""));
            }
        else
            {
            tmp_params_list.back().push_back(*it);
            }
        ++it;
        }
    
    commands[name](tmp_params_list);
    }

void cmd_processor::cmd_main()
    {
    char char_tmp;
    char type;
    int char_is_ready;
    std::string str_tmp;
    std::string name;
    std::string val;
    while(1)
        {
        if(IsFinished)
            return;
        str_tmp.clear();
        std::cout<<"> ";
        while(1)
            {
            char_is_ready=std::cin.peek();
            if(char_is_ready!=-1)
                {
                char_tmp=std::cin.get();
                if(char_tmp!='\n')
                    str_tmp.push_back(char_tmp);
                else
                    break;
                }
            
            }
        name.clear();
        val.clear();
        type=1;
        for(auto it=str_tmp.begin();it!=str_tmp.end();++it)
            {
            if(*it == '=')
                {
                type=0;
                ++it;
                while(it!=str_tmp.end())
                    {
                    val.push_back(*it);
                    ++it;
                    }
                break;
                }
            else if(*it=='(')
                {
                while(it!=str_tmp.end())
                    {
                    val.push_back(*it);
                    ++it;
                    }
                break;
                }
            else if(*it!=' ')
                name.push_back(*it);
            }
        if(type==0)
            variable_processor(name,val);
        else
            function_processor(name,val);
        }
    }

cmd_processor::cmd_processor()
    {
    IsFinished=false;
    commands.insert(std::pair<std::string,CmdCommand>("exit",
            std::bind(&cmd_processor::exit,this,std::placeholders::_1)));
    commands.insert(std::pair<std::string,CmdCommand>("variables",
            std::bind(&cmd_processor::print,this,std::placeholders::_1)));
    commands.insert(std::pair<std::string,CmdCommand>("help",
            std::bind(&cmd_processor::help,this,std::placeholders::_1)));
    }
cmd_processor::~cmd_processor()
    {
    }
void cmd_processor::add_variable(const std::string& name,const std::string type,void* ptr)
    {
    if(variables.find(name)!=variables.end())
        throw 1;
    else
        {
        if(type=="int")
            {
            variables.insert(std::pair<std::string,
                        std::shared_ptr<IVariable> >(name,
                        std::shared_ptr<IVariable>(new IntVariable((int*)ptr))));
            }
        else if(type=="double")
            {
            variables.insert(std::pair<std::string,
                        std::shared_ptr<IVariable> >(name,
                        std::shared_ptr<IVariable>(new DoubleVariable((double*)ptr))));
            }
        else
            throw 1;
        }
    }
void cmd_processor::add_variable(BaseContainer<IParameter>& in)
    {
    for(auto it=in.begin();it!=in.end();++it)
        {
        if(variables.find(it->GetName())!=variables.end())
            throw 1;
        variables.insert(std::pair<std::string,
                        std::shared_ptr<IVariable> >(it->GetName(),
                        std::shared_ptr<IVariable>(new IParameterVariable(it))));
        }
    }
void cmd_processor::add_command(const std::string name,const CmdCommand& com)
    {
    if(commands.find(name)!=commands.end())
        throw 1;
    commands.insert(std::pair<std::string,CmdCommand>(name,com));
    }

void cmd_processor::run()
    {
    cmd_main();
    }
void cmd_processor::arun()
    {
    }
