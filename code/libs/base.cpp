#include "CompManag_base.hpp"

using namespace CompManag;


ParameterBase::ParameterBase(const std::string& na,double* ptr):name(na),
                            param(ptr)
    {
    }

void ParameterBase::SetValue(const double& val)
    {
    *param=val;
    }

double ParameterBase::GetValue(void)
    {
    return *param;
    }
    
std::string ParameterBase::GetName(void)
    {
    return name;
    }

ParameterDescrBase::ParameterDescrBase(const std::string& na,const std::string& des):
                                name(na),descr(des)
    {
    }

inline std::string ParameterDescrBase::GetDescription(void) 
    {
    return descr;
    }

inline std::string ParameterDescrBase::GetName(void)
    {
    return name;
    }


OutputDescrBase::OutputDescrBase(const std::string& na,const std::string& des):
                            name(na),descr(des)
    {
    }

inline std::string OutputDescrBase::GetDescription(void)
    {
    return descr;
    }

inline std::string OutputDescrBase::GetName(void)
    {
    return name;
    }
/*********************************************************************
 * *******************************************************************/




task_description_holder::item_puter::item_puter(const std::string& label,const std::string& descr)
    {
    task_description_holder::Instance().descrs.insert(std::pair<std::string,std::string>(label,descr));
    }

task_description::task_description(const std::string& label)
    {
    it=task_description_holder::Instance().descrs.find(label);
    }

const std::string& task_description::to_string()
    {
    if(it!=task_description_holder::Instance().descrs.end())
        return it->second;
    else
        {
        static std::string void_string;
        return void_string;
        }
    }

/*********************************************************************
 * *******************************************************************/

