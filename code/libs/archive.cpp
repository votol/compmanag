#include "CompManag_archive.hpp"

using namespace CompManag::archive;


std::string& DataRecordVoid::GetShortDescription()
	{
	throw std::runtime_error("Trying to access not existing element");
	return str;
	}
std::string& DataRecordVoid::GetFullDescription()
	{
	throw std::runtime_error("Trying to access not existing element");
	return str;
	}
std::string& DataRecordVoid::GetLabel()
	{
	throw std::runtime_error("Trying to access not existing element");
	return str;
	}
CompManag::BaseContainer<CompManag::IParameter>& DataRecordVoid::GetParameters()
	{
	throw std::runtime_error("Trying to access not existing element");
	return param;
	}
CompManag::BaseContainer<CompManag::IOutputData>& DataRecordVoid::GetOutputs()
	{
	throw std::runtime_error("Trying to access not existing element");
	return data;
	}

/**********************************************************************
 * *******************************************************************/
BaseDataRecord::Parameter::Parameter(const std::string name, const double & value):par_name(name),par_value(value)
    {
    }
void BaseDataRecord::Parameter::SetValue(const double &value)
    {
    
    }
double BaseDataRecord::Parameter::GetValue(void)
    {
    return par_value;
    }
std::string BaseDataRecord::Parameter::GetName(void)
    {
    return par_name;
    }
BaseDataRecord::OutputData::OutputData(IOutputData& data)
    {
    data_name=data.GetName();
    dimensions=data.GetDimensions();
    data_data.resize(data.GetCount());
    auto it1=data.GetData().begin();
    for(auto it=data_data.begin();it!=data_data.end();++it)
        {
        *it=*it1;
        ++it1;
        }
    }

std::string BaseDataRecord::OutputData::GetName(void)
    {
    return data_name;
    }
unsigned int BaseDataRecord::OutputData::GetCount(void)
    {
    return data_data.size();
    }
const std::list<unsigned int>& BaseDataRecord::OutputData::GetDimensions(void)
    {
    return dimensions;
    }
std::vector<double>& BaseDataRecord::OutputData::GetData(void)
    {
    return data_data;
    }

BaseDataRecord::ParameterContainer::ParameterContainer(BaseContainer<IParameter> & params)
    {
    
    for(auto it=params.begin();it!=params.end();++it)
        {
        AddItem(it->GetName(),std::shared_ptr<IParameter>(new BaseDataRecord::Parameter(it->GetName(),it->GetValue())));
        }
    }


BaseDataRecord::OutputDataContainer::OutputDataContainer(BaseContainer<IOutputData> & data)
    {
    for(auto it=data.begin();it!=data.end();++it)
        {
        AddItem(it->GetName(),std::shared_ptr<IOutputData>(new BaseDataRecord::OutputData(*it)));
        }
    }

BaseDataRecord::OutputDataContainer::OutputDataContainer(BaseContainer<IOutputData> & data,CompManag::elist<std::string>& data_names)
    {
    for(auto it=data_names.begin();it!=data_names.end();++it)
        {
        AddItem(data[*it].GetName(),std::shared_ptr<IOutputData>(new BaseDataRecord::OutputData(data[*it])));
        }
    }

BaseDataRecord::BaseDataRecord(const std::string& label,BaseContainer<IParameter> & params, BaseContainer<IOutputData> & data):BaseDataRecord_label(label),
						ParameterContainerInstance(params),OutputDataContainerInstance(data)
    {
    
    }
BaseDataRecord::BaseDataRecord(const std::string& label,BaseContainer<IParameter> & params, BaseContainer<IOutputData> & data, CompManag::elist<std::string>&& data_names):BaseDataRecord_label(label),
						ParameterContainerInstance(params),OutputDataContainerInstance(data,data_names)
    {
    
    } 
std::string& BaseDataRecord::GetShortDescription()
    {
    return void_string;
    }
std::string& BaseDataRecord::GetFullDescription()
    {
    return void_string;
    }
std::string& BaseDataRecord::GetLabel()
    {
    return BaseDataRecord_label;
    }
CompManag::BaseContainer<CompManag::IParameter>& BaseDataRecord::GetParameters()
    {
    return ParameterContainerInstance;
    }
CompManag::BaseContainer<CompManag::IOutputData>& BaseDataRecord::GetOutputs()
    {
    return OutputDataContainerInstance;
    }

 /**********************************************************************
 * *******************************************************************/


archiver_v1::archiver_v1_DataRecord::Parameter::Parameter(const std::string name, const double & value):par_name(name),par_value(value)
    {
    }
void archiver_v1::archiver_v1_DataRecord::Parameter::SetValue(const double &value)
    {
    
    }
double archiver_v1::archiver_v1_DataRecord::Parameter::GetValue(void)
    {
    return par_value;
    }
std::string archiver_v1::archiver_v1_DataRecord::Parameter::GetName(void)
    {
    return par_name;
    }
archiver_v1::archiver_v1_DataRecord::OutputData::OutputData(IOutputData& data)
    {
    data_name=data.GetName();
    dimensions=data.GetDimensions();
    data_data.resize(data.GetCount());
    auto it1=data.GetData().begin();
    for(auto it=data_data.begin();it!=data_data.end();++it)
        {
        *it=*it1;
        ++it1;
        }
    }
archiver_v1::archiver_v1_DataRecord::OutputData::OutputData(const std::string& name,std::istream& ist)
    {
    data_name=name;
    int int_tmp,int_tmp1,int_tmp2;
    ist.read((char*)&(int_tmp),4);
    data_data.resize(int_tmp);
    ist.read((char*)&(int_tmp1),4);
    for(int count1=0;count1<int_tmp1;++count1)
        {
        ist.read((char*)&(int_tmp2),4);
        dimensions.push_back(int_tmp2);
        }
    ist.read((char*)data_data.data(),8*int_tmp);
    }

std::string archiver_v1::archiver_v1_DataRecord::OutputData::GetName(void)
    {
    return data_name;
    }
unsigned int archiver_v1::archiver_v1_DataRecord::OutputData::GetCount(void)
    {
    return data_data.size();
    }
const std::list<unsigned int>& archiver_v1::archiver_v1_DataRecord::OutputData::GetDimensions(void)
    {
    return dimensions;
    }
std::vector<double>& archiver_v1::archiver_v1_DataRecord::OutputData::GetData(void)
    {
    return data_data;
    }

archiver_v1::archiver_v1_DataRecord::ParameterContainer::ParameterContainer(BaseContainer<IParameter> & params)
    {
    
    for(auto it=params.begin();it!=params.end();++it)
        {
        AddItem(it->GetName(),std::shared_ptr<IParameter>(new archiver_v1::archiver_v1_DataRecord::Parameter(it->GetName(),it->GetValue())));
        }
    }
archiver_v1::archiver_v1_DataRecord::ParameterContainer::ParameterContainer(std::istream& ist)
    {
    int int_tmp,int_tmp1;
    int current_string_length=1; 
    double value;
    char* string_tmp=new char[1]; 
    ist.read((char*)&(int_tmp),4);
    for(int count1=0;count1<int_tmp;++count1)
		{
		ist.read((char*)&(int_tmp1),4);
		if(int_tmp1>current_string_length)
			{
			current_string_length=int_tmp1;
			delete [] string_tmp;
			string_tmp=new char[current_string_length];
			}
		ist.read(string_tmp,int_tmp1);
		ist.read((char*)&value,8);
		AddItem(std::string(string_tmp,int_tmp1),std::shared_ptr<IParameter>(new archiver_v1::archiver_v1_DataRecord::Parameter(std::string(string_tmp,int_tmp1),value)));
		}
    delete [] string_tmp;
    }

archiver_v1::archiver_v1_DataRecord::OutputDataContainer::OutputDataContainer(BaseContainer<IOutputData> & data)
    {
    for(auto it=data.begin();it!=data.end();++it)
        {
        AddItem(it->GetName(),std::shared_ptr<IOutputData>(new archiver_v1::archiver_v1_DataRecord::OutputData(*it)));
        }
    }
archiver_v1::archiver_v1_DataRecord::OutputDataContainer::OutputDataContainer(std::istream& ist)
	{
	int int_tmp,int_tmp1;
    int current_string_length=1; 
    char* string_tmp=new char[1]; 
    ist.read((char*)&(int_tmp),4);
    for(int count1=0;count1<int_tmp;++count1)
		{
		ist.read((char*)&(int_tmp1),4);
		if(int_tmp1>current_string_length)
			{
			current_string_length=int_tmp1;
			delete [] string_tmp;
			string_tmp=new char[current_string_length];
			}
		ist.read(string_tmp,int_tmp1);
		AddItem(std::string(string_tmp,int_tmp1),std::shared_ptr<IOutputData>(new archiver_v1::archiver_v1_DataRecord::OutputData(std::string(string_tmp,int_tmp1),ist)));
		}
	delete [] string_tmp;
	}

archiver_v1::archiver_v1_DataRecord::archiver_v1_DataRecord(archiver_v1* ptr,const std::string& label,BaseContainer<IParameter> & params, BaseContainer<IOutputData> & data):parent(ptr),
                                archiver_v1_DataRecord_label(label),ParameterContainerInstance(params),OutputDataContainerInstance(data)
    {
    
    }
archiver_v1::archiver_v1_DataRecord::archiver_v1_DataRecord(archiver_v1* ptr,const std::string& label,std::istream& ist):parent(ptr),
                                archiver_v1_DataRecord_label(label),ParameterContainerInstance(ist),OutputDataContainerInstance(ist)
    {
    
    }

std::string& archiver_v1::archiver_v1_DataRecord::GetShortDescription()
    {
    return parent->task_short_description;
    }
std::string& archiver_v1::archiver_v1_DataRecord::GetFullDescription()
    {
    return parent->task_full_description;
    }
std::string& archiver_v1::archiver_v1_DataRecord::GetLabel()
    {
    return archiver_v1_DataRecord_label;
    }
CompManag::BaseContainer<CompManag::IParameter>& archiver_v1::archiver_v1_DataRecord::GetParameters()
    {
    return ParameterContainerInstance;
    }
CompManag::BaseContainer<CompManag::IOutputData>& archiver_v1::archiver_v1_DataRecord::GetOutputs()
    {
    return OutputDataContainerInstance;
    }
archiver_v1::archiver_v1_DataContainerLabel::iterator::iterator(const std::list<archiver_v1_DataRecord>::iterator& in):it(in)
    {
    }
archiver_v1::archiver_v1_DataContainerLabel::iterator::iterator(const iterator& in)
    {
    it=in.it;
    }
base_iterator<IDataRecord>& archiver_v1::archiver_v1_DataContainerLabel::iterator::operator=(const base_iterator<IDataRecord>& in)
    {
    const archiver_v1::archiver_v1_DataContainerLabel::iterator* tmp=dynamic_cast<const archiver_v1::archiver_v1_DataContainerLabel::iterator*>(&in);
    it=tmp->it;
    return *this;
    }
bool archiver_v1::archiver_v1_DataContainerLabel::iterator::operator==(const base_iterator<IDataRecord>& in)
    {
    const archiver_v1::archiver_v1_DataContainerLabel::iterator* tmp=dynamic_cast<const archiver_v1::archiver_v1_DataContainerLabel::iterator*>(&in);
    return it==tmp->it;
    }
bool archiver_v1::archiver_v1_DataContainerLabel::iterator::operator!=(const base_iterator<IDataRecord>& in)
    {
    const archiver_v1::archiver_v1_DataContainerLabel::iterator* tmp=dynamic_cast<const archiver_v1::archiver_v1_DataContainerLabel::iterator*>(&in);
    return it!=tmp->it;
    }
IDataRecord& archiver_v1::archiver_v1_DataContainerLabel::iterator::operator*()
    {
    return *it;
    }
IDataRecord* archiver_v1::archiver_v1_DataContainerLabel::iterator::operator->()
    {
    return &(*it);
    }
base_iterator<IDataRecord>& archiver_v1::archiver_v1_DataContainerLabel::iterator::operator++()
    {
    ++it;
    return *this;
    }
base_iterator<IDataRecord>& archiver_v1::archiver_v1_DataContainerLabel::iterator::operator--()
    {
    --it;
    return *this;
    }
std::shared_ptr<base_iterator<IDataRecord> > archiver_v1::archiver_v1_DataContainerLabel::iterator::copy(void)const
	{
	return std::shared_ptr<base_iterator<IDataRecord> >(new archiver_v1::archiver_v1_DataContainerLabel::iterator(*this));
	}

archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator::iterator(const std::list<std::list<archiver_v1_DataRecord>::iterator>::iterator& in):it(in)
    {
    }
archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator::iterator(const iterator& in)  
    {
    it=in.it;
    }
base_iterator<IDataRecord>& archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator::operator=(const base_iterator<IDataRecord>& in)
    {
    const archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator* tmp=
				dynamic_cast<const archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator*>(&in);
    it=tmp->it;
    return *this;
    }
bool archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator::operator==(const base_iterator<IDataRecord>& in)
    {
    const archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator* tmp=
				dynamic_cast<const archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator*>(&in);
    return it==tmp->it;
    }
bool archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator::operator!=(const base_iterator<IDataRecord>& in)
    {
    const archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator* tmp=
				dynamic_cast<const archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator*>(&in);
    return it!=tmp->it;
    }
IDataRecord& archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator::operator*()
    {
    return *(*it);
    }
IDataRecord* archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator::operator->()
    {
    return &(*(*it));
    }
base_iterator<IDataRecord>& archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator::operator++()
    {
    ++it;
    return *this;
    }
base_iterator<IDataRecord>& archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator::operator--()
    {
    --it;
    return *this;
    }
std::shared_ptr<base_iterator<IDataRecord> > archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator::copy(void)const
	{
    return std::shared_ptr<base_iterator<IDataRecord> >(new archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator(*this));
	}
archiver_v1::archiver_v1_DataContainerLabel::DataContainer::DataContainer(archiver_v1* ptr)
    {
    
    }
std::string& archiver_v1::archiver_v1_DataContainerLabel::DataContainer::GetShortDescription()
    {
    return parent->task_short_description;
    }
std::string& archiver_v1::archiver_v1_DataContainerLabel::DataContainer::GetFullDescription()
    {
    return parent->task_full_description;
    }
base_iterator<IDataRecord> archiver_v1::archiver_v1_DataContainerLabel::DataContainer::begin()
    {
    return archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator(data_iterators.begin());
    }
base_iterator<IDataRecord> archiver_v1::archiver_v1_DataContainerLabel::DataContainer::end()
    {
    return archiver_v1::archiver_v1_DataContainerLabel::DataContainer::iterator(data_iterators.end());
    }
long unsigned int archiver_v1::archiver_v1_DataContainerLabel::DataContainer::size()
    {
    return data_iterators.size();
    }
void archiver_v1::archiver_v1_DataContainerLabel::DataContainer::push_back(const std::list<archiver_v1_DataRecord>::iterator& in)
    {
    data_iterators.push_back(in);
    }

archiver_v1::archiver_v1_DataContainerLabel::label_not_exists::label_not_exists(const std::string& in):invalid_argument(""),name(in)
    {
    }
archiver_v1::archiver_v1_DataContainerLabel::label_not_exists::~label_not_exists()
    {
    }
const char* archiver_v1::archiver_v1_DataContainerLabel::label_not_exists::what() const noexcept
    {
    return ("label not exists: "+name).c_str();
    }

archiver_v1::archiver_v1_DataContainerLabel::archiver_v1_DataContainerLabel(archiver_v1* ptr):parent(ptr)
    {
    }
std::string& archiver_v1::archiver_v1_DataContainerLabel::GetShortDescription()
    {
    return parent->task_short_description;
    }
std::string& archiver_v1::archiver_v1_DataContainerLabel::GetFullDescription()
    {
    return parent->task_full_description;
    }
base_iterator<IDataRecord> archiver_v1::archiver_v1_DataContainerLabel::begin()
    {
    return archiver_v1::archiver_v1_DataContainerLabel::iterator(archiver_v1_DataContainerLabel_data.begin());
    }
base_iterator<IDataRecord> archiver_v1::archiver_v1_DataContainerLabel::end()
    {
    return archiver_v1::archiver_v1_DataContainerLabel::iterator(archiver_v1_DataContainerLabel_data.end());
    }
long unsigned int archiver_v1::archiver_v1_DataContainerLabel::size()
    {
    return archiver_v1_DataContainerLabel_data.size();
    }
IDataContainer& archiver_v1::archiver_v1_DataContainerLabel::operator[](const std::string& in)
    {
    auto it=archiver_v1_DataContainerLabel_data_ptrs.find(in);
    if(it==archiver_v1_DataContainerLabel_data_ptrs.end())
        throw label_not_exists(in);
    return it->second; 
    }
std::list<archiver_v1::archiver_v1_DataRecord>::iterator archiver_v1::archiver_v1_DataContainerLabel::add_data(const std::string& label,BaseContainer<IParameter> & params, BaseContainer<IOutputData> & data)
	{
	archiver_v1_DataContainerLabel_data.push_back(archiver_v1_DataRecord(parent,label,params,data));
	auto it=archiver_v1_DataContainerLabel_data_ptrs.find(label);
	if(it==archiver_v1_DataContainerLabel_data_ptrs.end())
		{
		it=(archiver_v1_DataContainerLabel_data_ptrs.insert(std::pair<std::string, DataContainer>(label,DataContainer(parent)))).first;
		}
	it->second.push_back(--(archiver_v1_DataContainerLabel_data.end()));
	return --(archiver_v1_DataContainerLabel_data.end());
    }
void archiver_v1::archiver_v1_DataContainerLabel::add_data(std::istream& ist)
	{
	int int_tmp;
    ist.read((char*)&(int_tmp),4);
    char* label=new char[int_tmp]; 
    ist.read(label,int_tmp);
    
    archiver_v1_DataContainerLabel_data.push_back(archiver_v1_DataRecord(parent,std::string(label,int_tmp),ist));
	auto it=archiver_v1_DataContainerLabel_data_ptrs.find(std::string(label,int_tmp));
	if(it==archiver_v1_DataContainerLabel_data_ptrs.end())
		{
		it=(archiver_v1_DataContainerLabel_data_ptrs.insert(std::pair<std::string, DataContainer>(std::string(label,int_tmp),DataContainer(parent)))).first;
		}
	it->second.push_back(--(archiver_v1_DataContainerLabel_data.end()));
    delete [] label;
	}
archiver_v1::archiver_v1_DataWriter::archiver_v1_DataWriter(archiver_v1* ptr):parent(ptr)
    {
    }
void archiver_v1::archiver_v1_DataWriter::AddRecord(const std::string& label,BaseContainer<IParameter> & params, BaseContainer<IOutputData> & data)
    {
    parent->archiver_v1_DataContainerLabel_instance_mutex.lock();
    auto it=parent->archiver_v1_DataContainerLabel_instance->add_data(label,params,data);
    parent->archiver_v1_DataContainerLabel_instance_mutex.unlock();
    parent->list_of_records_for_writing_mutex.lock();
    parent->list_of_records_for_writing.push_back(it);
    parent->list_of_records_for_writing_mutex.unlock();
    }

void archiver_v1::read_description_from_file(std::istream& ist)
    {
    int int_tmp;
    int current_string_length=1; 
    char* string_tmp=new char[1]; 
    ist.read((char*)&(int_tmp),4);
    if(int_tmp>current_string_length)
        {
        current_string_length=int_tmp;
        delete [] string_tmp;
        string_tmp=new char[current_string_length];
        }
    ist.read(string_tmp,int_tmp);
    task_short_description=std::string(string_tmp,int_tmp);
    ist.read((char*)&(int_tmp),4);
    if(int_tmp>current_string_length)
        {
        current_string_length=int_tmp;
        delete [] string_tmp;
        string_tmp=new char[current_string_length];
        }
    ist.read(string_tmp,int_tmp);
    task_full_description=std::string(string_tmp,int_tmp);
    delete [] string_tmp;
    }
void archiver_v1::write_to_file_record(archiver_v1_DataRecord& rec)
    {
    std::ofstream ofs;
    ofs.open(archiver_file_name,std::ofstream::binary | std::ofstream::app);
    int int_tmp,int_tmp1;
    double double_tmp;
    std::list<unsigned int> dimensions_tmp;
    std::string str_tmp;
    int_tmp=rec.GetLabel().size();
    ofs.write((char*)&int_tmp,4);
    ofs.write(rec.GetLabel().data(),int_tmp);
    int_tmp=rec.GetParameters().size();
    ofs.write((char*)&int_tmp,4);
    for(auto it=rec.GetParameters().begin();it!=rec.GetParameters().end();++it)
        {
        str_tmp=it->GetName();
        int_tmp=str_tmp.size();
        ofs.write((char*)&int_tmp,4);
        ofs.write(str_tmp.data(),int_tmp);
        double_tmp=it->GetValue();
        ofs.write((char*)&double_tmp,8);
        }
    int_tmp=rec.GetOutputs().size();
    ofs.write((char*)&int_tmp,4);
    for(auto it=rec.GetOutputs().begin();it!=rec.GetOutputs().end();++it)
        {
        str_tmp=it->GetName();
        int_tmp=str_tmp.size();
        ofs.write((char*)&int_tmp,4);
        ofs.write(str_tmp.data(),int_tmp);
        int_tmp=it->GetCount();
        ofs.write((char*)&int_tmp,4);
        dimensions_tmp=it->GetDimensions();
        int_tmp1=dimensions_tmp.size();
        ofs.write((char*)&int_tmp1,4);
        for(auto it1=dimensions_tmp.begin();it1!=dimensions_tmp.end();++it1)
            {
            int_tmp1=*it1;
            ofs.write((char*)&int_tmp1,4);
            }
        ofs.write((char*)it->GetData().data(),8*int_tmp);
        }
    ofs.close();
    }

void archiver_v1::file_writing_function()
    {
    while(1)
        {
        while(1)
            {
            list_of_records_for_writing_mutex.lock();
            if(list_of_records_for_writing.size()==0)
                {
                list_of_records_for_writing_mutex.unlock();
                break;
                }
            write_to_file_record(*(list_of_records_for_writing.front()));
            list_of_records_for_writing.pop_front();
            list_of_records_for_writing_mutex.unlock();
            }
        is_working_mutex.lock();
        if(!is_working)
            {
            is_working_mutex.unlock();
            break;
            }
        is_working_mutex.unlock();
        std::this_thread::sleep_for (std::chrono::seconds(1));
        }
    }

archiver_v1::archiver_v1(const std::string& file_name):archiver_file_name(file_name)
    {
	if ( !boost::filesystem::exists( archiver_file_name ) )
		throw std::runtime_error("Can't create archiver:file "+archiver_file_name+" not exists");
    std::ifstream ifs;
    ifs.open(archiver_file_name,std::ifstream::binary);
        {
        char uuid_from_file[UUID_SIZE];
        ifs.read(uuid_from_file,UUID_SIZE);
        if(std::string(uuid_from_file,UUID_SIZE)!=std::string(UUID))
            throw std::runtime_error("Can't create archiver: bad file type: "+archiver_file_name);
        unsigned int version_id;
        ifs.read((char*)&version_id,4);
        if(version_id!=1)
            throw std::runtime_error("Can't create archiver: wrong version of archiver: "+archiver_file_name);
        }
    read_description_from_file(ifs);
    archiver_v1_DataContainerLabel_instance=new archiver_v1_DataContainerLabel(this);
    ifs.peek();
    while(!ifs.eof())
        {
        archiver_v1_DataContainerLabel_instance->add_data(ifs);
        ifs.peek();
        }
    ifs.close();
    is_working=true;
    saving_to_file_thread=std::thread(&archiver_v1::file_writing_function,this);
    }
archiver_v1::archiver_v1(const std::string& file_name,const std::string & short_descr,const std::string & full_descr):archiver_file_name(file_name)
    {
    archiver_v1_DataContainerLabel_instance=new archiver_v1_DataContainerLabel(this);
    if ( boost::filesystem::exists( archiver_file_name ) )
		{
        std::ifstream ifs;
        ifs.open(archiver_file_name,std::ifstream::binary);
            {
            char uuid_from_file[UUID_SIZE];
            ifs.read(uuid_from_file,UUID_SIZE);
            if(std::string(uuid_from_file,UUID_SIZE)!=std::string(UUID))
                throw std::runtime_error("Can't create archiver: bad file type: "+archiver_file_name);
            unsigned int version_id;
            ifs.read((char*)&version_id,4);
            if(version_id!=1)
                throw std::runtime_error("Can't create archiver: wrong version of archiver: "+archiver_file_name);
            }
        read_description_from_file(ifs);
        if(short_descr!=task_short_description || full_descr!=task_full_description)
            throw std::runtime_error("Can't create archiver: file exists but has wrong description");
        ifs.peek();
        while(!ifs.eof())
            {
            archiver_v1_DataContainerLabel_instance->add_data(ifs);
            ifs.peek();
            }
        ifs.close();
        }
    else
        {
        task_short_description=short_descr;
        task_full_description=full_descr;
        std::ofstream ofs;
        ofs.open(archiver_file_name,std::ofstream::binary);
        int int_tmp;
        ofs.write(UUID,UUID_SIZE);
        int_tmp=1;
        ofs.write((char*)&int_tmp,4);
        int_tmp=short_descr.size();
        ofs.write((char*)&int_tmp,4);
        ofs.write(short_descr.data(),int_tmp);
        int_tmp=full_descr.size();
        ofs.write((char*)&int_tmp,4);
        ofs.write(full_descr.data(),int_tmp);
        ofs.close();
        }
    is_working=true;
    saving_to_file_thread=std::thread(&archiver_v1::file_writing_function,this);
    }
archiver_v1::~archiver_v1()
    {
    is_working_mutex.lock();
    is_working=false;
    is_working_mutex.unlock();
    saving_to_file_thread.join();
    if(archiver_v1_DataContainerLabel_instance!=0)
		delete archiver_v1_DataContainerLabel_instance;
    }
IDataContainerLabel& archiver_v1::GetData()
    {
    return *archiver_v1_DataContainerLabel_instance;
    }
std::shared_ptr<IDataWriter> archiver_v1::GetWriterInstance()
    {
    return std::shared_ptr<IDataWriter>(new archiver_v1_DataWriter(this));
    }
