#include <iostream>
#include <boost/tuple/tuple.hpp>
extern "C" {
#include <quadmath.h>
}
#include "complex.h"
#include "CompManag.hpp"

using namespace CompManag;

make_description(label,
class MyCalculator:public ICalculator
    {
    private:
        class ParametrContainer:public BaseContainer<IParameter>
            {
            public:
                ParametrContainer(MyCalculator *in)
                    {
                    AddItem("gam",std::shared_ptr<IParameter>(new ParameterBase("gam",&in->gam)));
                    AddItem("eps",std::shared_ptr<IParameter>(new ParameterBase("eps",&in->epsm)));
                    AddItem("nt",std::shared_ptr<IParameter>(new ParameterBase("nt",&in->nt)));
                    AddItem("g",std::shared_ptr<IParameter>(new ParameterBase("g",&in->g)));
                    AddItem("D",std::shared_ptr<IParameter>(new ParameterBase("D",&in->D)));
                    AddItem("teta",std::shared_ptr<IParameter>(new ParameterBase("teta",&in->teta)));
                    AddItem("dt",std::shared_ptr<IParameter>(new ParameterBase("dt",&in->dt)));
                    AddItem("Nmax",std::shared_ptr<IParameter>(new ParameterBase("Nmax",&in->Nmax)));
                    AddItem("dN",std::shared_ptr<IParameter>(new ParameterBase("dN",&in->dN)));
                    }
            
                ~ParametrContainer(){};
            };
        
        class OutputOne:public IOutputData
            {
            private:
                MyCalculator * parent;
                unsigned int out_id;
                std::string name;
                std::list<unsigned int> dimensions;
            public:
                OutputOne(const std::string &na,MyCalculator *p,unsigned int id):
                                        parent(p),out_id(id),name(na)
                    {
                    dimensions.push_back(parent->OutputNum);
                    }
                virtual ~OutputOne(){};
                virtual std::string GetName(void)
                    {
                    return name;
                    }
                
                virtual unsigned int GetCount(void)
                    {
                    return parent->OutputNum;
                    }
                virtual const std::list<unsigned int>& GetDimensions(void)
                    {
                    return dimensions;
                    }
                
                virtual std::vector<double>& GetData(void)
                    {
                    return parent->output[out_id];
                    }
            };
        
        class OutputContainer:public BaseContainer<IOutputData>
            {
            public:    
                OutputContainer(MyCalculator *in)
                    {
                    AddItem("t",std::shared_ptr<IOutputData>
                    (new OutputOne("t",in,0)));
                    AddItem("I1",std::shared_ptr<IOutputData>
                    (new OutputOne("I1",in,1)));
                    AddItem("I2",std::shared_ptr<IOutputData>
                    (new OutputOne("I2",in,2)));
                    AddItem("ak1*a2_re",std::shared_ptr<IOutputData>
                    (new OutputOne("ak1*a2_re",in,3)));
                    AddItem("ak1*a2_im",std::shared_ptr<IOutputData>
                    (new OutputOne("ak1*a2_im",in,4)));
                    AddItem("alph",std::shared_ptr<IOutputData>
                    (new OutputOne("alph",in,5)));
                    AddItem("a1*a2_re",std::shared_ptr<IOutputData>
                    (new OutputOne("a1*a2_re",in,6)));
                    AddItem("a1*a2_im",std::shared_ptr<IOutputData>
                    (new OutputOne("a1*a2_im",in,7)));
                    AddItem("alph1",std::shared_ptr<IOutputData>
                    (new OutputOne("alph1",in,8)));
                    AddItem("En",std::shared_ptr<IOutputData>
                    (new OutputOne("En",in,9)));
                    }
                ~OutputContainer(){};
            };
        
        
        
        
        unsigned int OutputNum;
        Runge::RungeSolver<complex<double> > runge;
        Runge::RungeSolverInit<complex<double> > runge_params;
        
        
        double gam;
        
        double eps;
        double epsm;
        
        double g;
        double nt;
        double D;
        double teta;
        double dt;
        double Nmax;
        double dN;
        
        
        //tmp
        __float128 Sigm;
        std::vector<__float128> Mat16;
        std::vector<__float128> Mat9;
        std::vector<__float128> Mat4;
        __float128 Det;
        
        ParametrContainer params_pointer;
        OutputContainer outputs;
    
        std::vector<std::vector<double> > output;
    
        __float128 Det4(void)
            {
            return Mat4[0]*Mat4[3]-Mat4[1]*Mat4[2];
            }
        
        
        __float128 Det9(void)
            {
            __float128 sum=0;
            __float128 koe=1;
            for(unsigned int i=0;i<3;i++)
                {
                unsigned int l=0;
                for(unsigned int j=0;j<3;j++)
                    for(unsigned int k=1;k<3;k++)
                        if(j!=i)
                            {
                            Mat4[l]=Mat9[3*k+j];
                            l++;
                            }
                sum+=Mat9[i]*koe*Det4();
                koe*=-1;
                }
            return sum;
            }
        
        __float128 Det16(void)
            {
            __float128 sum=0;
            __float128 koe=1;
            for(unsigned int i=0;i<4;i++)
                {
                unsigned int l=0;
                for(unsigned int j=0;j<4;j++)
                    for(unsigned int k=1;k<4;k++)
                        if(j!=i)
                            {
                            Mat9[l]=Mat16[4*k+j];
                            l++;
                            }
                sum+=Mat16[i]*koe*Det9();
                koe*=-1;
                }
            return sum;
            }
        
        
        void Init(complex<double> * in)
            {
            in[0]=complex<double>(0.0,0.0);
            in[1]=complex<double>(0.0,0.0);
            in[2]=complex<double>(0.0,0.0);
            
            in[3]=complex<double>(0.0,0.0);
            in[4]=complex<double>(0.0,0.0);
            in[5]=complex<double>(0.0,0.0);
            
            in[6]=complex<double>(0.0,0.0);
            in[7]=complex<double>(0.0,0.0);
            in[8]=complex<double>(0.0,0.0);
            in[9]=complex<double>(0.0,0.0);
            
            in[10]=complex<double>(0.0,0.0);
            in[11]=complex<double>(0.0,0.0);
            in[12]=complex<double>(0.0,0.0);
            in[13]=complex<double>(0.0,0.0);
            in[14]=complex<double>(0.0,0.0);
            in[15]=complex<double>(0.0,0.0);
            }
        
        void Derivative(const complex<double>* in,complex<double>* out,const double t)
            {
            eps=epsm;
            out[0]=-gam*in[0]+2.0*eps*in[2].__im+gam*nt;
            out[1]=-gam*in[1]+2.0*eps*in[2].__im+gam*nt;
            out[2]=complex<double>(-gam-D,-2.0*teta)*in[2]+complex<double>(0.0,eps)*(in[0]+in[1]+1.0);
            
            out[3]=-gam*in[3]+complex<double>(0.0,eps)*(conj(in[4])-in[5]);
            out[4]=complex<double>(-(gam+D),-2*teta)*in[4]+complex<double>(0.0,2.0*eps)*conj(in[3]);
            out[5]=complex<double>(-(gam+D),-2*teta)*in[5]+complex<double>(0.0,2.0*eps)*in[3];
            
            out[6]=complex<double>(-gam,-2.0*teta)*in[6]+complex<double>(0.0,eps)*(in[7]+in[8]+exp(-D*t));
            out[7]=(-gam-D)*in[7]+complex<double>(0.0,eps)*(in[9]-in[6])+gam*nt*exp(-D*t);
            out[8]=(-gam-D)*in[8]+complex<double>(0.0,eps)*(in[9]-in[6])+gam*nt*exp(-D*t);
            out[9]=complex<double>(-gam-4.0*D,2.0*teta)*in[9]-complex<double>(0.0,eps)*(in[7]+in[8]+exp(-D*t));
            
            out[10]=complex<double>(-gam,-2*teta)*in[10]+complex<double>(0.0,2.0*eps)*conj(in[13]);
            out[11]=complex<double>(-gam,-2*teta)*in[11]+complex<double>(0.0,2.0*eps)*in[12];
            out[12]=-(gam+D)*in[12]-complex<double>(0.0,eps)*(in[11]-conj(in[14]));
            out[13]=-(gam+D)*in[13]-complex<double>(0.0,eps)*(in[15]-conj(in[10]));
            out[14]=complex<double>(-gam-4.0*D,-2*teta)*in[14]+complex<double>(0.0,2.0*eps)*conj(in[12]);
            out[15]=complex<double>(-gam-4.0*D,-2*teta)*in[15]+complex<double>(0.0,2.0*eps)*in[13];
            }
    
        bool Output(const complex<double>* in,const double t)
            {
            if(in[0].__re+in[1].__re>1e10)
                {
                std::cout<<OutputNum<<std::endl;
                return false;
                }
            output[0][OutputNum]=gam*t;///0.0005;
            output[1][OutputNum]=in[0].__re;
            output[2][OutputNum]=in[1].__re;
            output[3][OutputNum]=in[3].__re;
            output[4][OutputNum]=in[3].__im;
            output[5][OutputNum]=2*sqrt(in[3].__im*in[3].__im+in[3].__re*in[3].__re)/(in[0].__re+in[1].__re);
            output[6][OutputNum]=in[6].__re;
            output[7][OutputNum]=in[6].__im;
            output[8][OutputNum]=2*sqrt(in[6].__im*in[6].__im+in[6].__re*in[6].__re)/(in[0].__re+in[1].__re);
            
            Sigm=__float128(in[0].__re+0.5)*__float128(in[0].__re+0.5)+__float128(in[1].__re+0.5)*__float128(in[1].__re+0.5)+
                    2.0*(__float128(in[6].__im)*__float128(in[6].__im)+__float128(in[6].__re)*__float128(in[6].__re))-
                    2.0*(__float128(in[3].__im)*__float128(in[3].__im)+__float128(in[3].__re)*__float128(in[3].__re))-
                    (__float128(in[10].__im)*__float128(in[10].__im)+__float128(in[10].__re)*__float128(in[10].__re))-
                    (__float128(in[11].__im)*__float128(in[11].__im)+__float128(in[11].__re)*__float128(in[11].__re));
                    
            Mat16[0]=in[0].__re+0.5-in[10].__re;
            Mat16[1]=-in[10].__im;
            Mat16[2]=-in[6].__re+in[3].__re;
            Mat16[3]=-in[6].__im+in[3].__im;
            Mat16[4]=-in[10].__im;
            Mat16[5]=in[0].__re+0.5+in[10].__re;
            Mat16[6]=-in[6].__im-in[3].__im;
            Mat16[7]=in[6].__re+in[3].__re;
            Mat16[8]=-in[6].__re+in[3].__re;
            Mat16[9]=-in[6].__im-in[3].__im;
            Mat16[10]=in[1].__re+0.5-in[11].__re;
            Mat16[11]=-in[11].__im;
            Mat16[12]=-in[6].__im+in[3].__im;
            Mat16[13]=in[6].__re+in[3].__re;
            Mat16[14]=-in[11].__im;
            Mat16[15]=in[1].__re+0.5+in[11].__re;
            Det=Det16();
            output[9][OutputNum]=-log(double(2*sqrtq(0.5*(Sigm-sqrtq(Sigm*Sigm-4*Det)))))/log(2.0);//-log(2*sqrt(0.5*(Sigm+sqrt(Sigm*Sigm-4*Det))));
            if(output[9][OutputNum]<0)
                output[9][OutputNum]=0.0;
            OutputNum++;
            return true;
            }
    
    public:
        MyCalculator():params_pointer(this),outputs(this)
            {
            Mat16.resize(16);
            Mat9.resize(9);
            Mat4.resize(4);
            
            OutputNum=0;
            output.resize(10);
            runge_params.Dimension=16;
            runge_params.Init=std::bind(&MyCalculator::Init,this,std::placeholders::_1);
            runge_params.Derivative=std::bind(&MyCalculator::Derivative,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3);
            runge_params.OutputMaker=std::bind(&MyCalculator::Output,this,std::placeholders::_1,std::placeholders::_2);
            }
        
        virtual ~MyCalculator(){};
        
        virtual BaseContainer<IParameter> & params(void)
            {
            return params_pointer;
            }
        
        virtual BaseContainer<IOutputData> & calculate(void)
            {
            OutputNum=0;
            for(auto it=output.begin();it!=output.end();++it)
                it->resize(int(Nmax/dN));
            
            runge_params.NumTimeSteps=int(Nmax);
            runge_params.TimeStep=dt;
            runge_params.OutputNumTimeSteps=int(dN);
            runge.Init(runge_params);
            runge.Calculate();
            return outputs;
            }
    };
)

class MyCalculatorFabric:ICalculatorFabric
    {
    private:
        class ParameterDescrCont:public BaseContainer<IParameterDescr>
            {
            public:
                ParameterDescrCont()
                    {
                    AddItem("gam",std::shared_ptr<IParameterDescr>
                            (new ParameterDescrBase("gam",
                            "interaction power whith the bath")));
                    AddItem("eps",std::shared_ptr<IParameterDescr>
                            (new ParameterDescrBase("eps",
                            "parametric pumping power")));
                    AddItem("nt",std::shared_ptr<IParameterDescr>
                            (new ParameterDescrBase("nt",
                            "Number of equlibrium photons")));
                    AddItem("D",std::shared_ptr<IParameterDescr>
                            (new ParameterDescrBase("D",
                            "Spectral noise width of the pumping")));
                    AddItem("teta",std::shared_ptr<IParameterDescr>
                            (new ParameterDescrBase("teta",
                            "Frequency detuning nu-om")));
                    AddItem("dt",std::shared_ptr<IParameterDescr>
                            (new ParameterDescrBase("dt",
                            "Time step of Runge-Kutt method")));
                    AddItem("Nmax",std::shared_ptr<IParameterDescr>
                            (new ParameterDescrBase("Nmax",
                            "Number of steps of Runge-Kutt solver")));
                    AddItem("dN",std::shared_ptr<IParameterDescr>
                            (new ParameterDescrBase("dN",
                            "Number of steps through which output is calculating")));
                    }
                ~ParameterDescrCont(){};
            };
    
        class OutputDescrCont:public BaseContainer<IOutputDescr>
            {
            public:    
                OutputDescrCont()
                    {
                    AddItem("t",std::shared_ptr<IOutputDescr>
                            (new OutputDescrBase("t",
                            "Time")));
                    AddItem("I1",std::shared_ptr<IOutputDescr>
                            (new OutputDescrBase("I1",
                            "Number of quants in the first oscillator")));
                    AddItem("I2",std::shared_ptr<IOutputDescr>
                            (new OutputDescrBase("I2",
                            "Number of quants in the second oscillator")));
                    AddItem("ak1*a2_re",std::shared_ptr<IOutputDescr>
                            (new OutputDescrBase("ak1*a2_re",
                            "Real part of the correlator ak1*a2")));
                    AddItem("ak1*a2_im",std::shared_ptr<IOutputDescr>
                            (new OutputDescrBase("ak1*a2_im",
                            "Imaginary part of the correlator ak1*a2")));
                    AddItem("alph",std::shared_ptr<IOutputDescr>
                            (new OutputDescrBase("alph",
                            "Entanglement measure")));
                    }
                ~OutputDescrCont(){};
            };
        
        
        ParameterDescrCont parameter_descr;
        OutputDescrCont output_descr;
        std::string descr;
    
    public:
        MyCalculatorFabric()
            {
            }
        ~MyCalculatorFabric(){};
        virtual BaseContainer<IParameterDescr> & get_parametr_descr(void)
            {
            return parameter_descr;
            }
        virtual BaseContainer<IOutputDescr> & get_output_descr(void)
            {
            return output_descr;
            }
        virtual std::shared_ptr<ICalculator> get_calculator_instance(void)
            {
            return std::shared_ptr<ICalculator>(new MyCalculator);
            }
    };


void test_cmd_command_fun(std::list<std::string> in)
    {
    for(auto it=in.begin();it!=in.end();++it)
        {
        std::cout<<*it<<std::endl;
        }
    }


int main(int argc,char ** argv)
    {
    MyCalculator *a;
    BaseContainer<IOutputData> *out_tmp;
    a=new MyCalculator();
    a->params()["gam"].SetValue(1.0);
    a->params()["eps"].SetValue(500.0);
    a->params()["nt"].SetValue(10.001);
    a->params()["g"].SetValue(0.1);
    a->params()["D"].SetValue(1e-10/0.00005);
    a->params()["teta"].SetValue(0.00);
    a->params()["dt"].SetValue(0.00001);
    a->params()["Nmax"].SetValue(10000000.0);
    a->params()["dN"].SetValue(10.0);
    out_tmp=&(a->calculate());
    
    archive::BaseDataRecord data_record("",a->params(),*out_tmp,elist<std::string>().push_back("t")("En"));
    
    archive::archiver_v1  archive_testing("tests/archive.bin","test archive",task_description("label").to_string());
    std::shared_ptr<archive::IDataWriter> writing_instance=archive_testing.GetWriterInstance();
    //std::cout<<archive_testing.GetData()["asdafdqf"].begin()->GetLabel()<<std::endl;
    
    cmd_processor cmd_inst;
    cmd_inst.add_variable(a->params());
    cmd_inst.add_command("test_command",[a,out_tmp,writing_instance](std::list<std::string> in)mutable{out_tmp=&(a->calculate());
                            writing_instance->AddRecord("",a->params(),*out_tmp);});
    cmd_inst.add_command("test_params",test_cmd_command_fun);
    cmd_inst.run();
    
    delete a;
    
    std::cout<<"Everything is good"<<std::endl;
    return 0;
    }
