#ifndef __CompManag_base_hpp_
#define __CompManag_base_hpp_

#include <iostream>


#include <boost/preprocessor/cat.hpp>
#include <memory>
#include <string>
#include <map>
#include <vector>
#include <list>
#include <functional>

namespace CompManag
{

class IParameter
	{
	public:
		IParameter(){};
		virtual ~IParameter(){};
		virtual void SetValue(const double &)=0;
        virtual double GetValue(void)=0;
		virtual std::string GetName(void)=0;
	};

class IParameterDescr
	{
	public:
		IParameterDescr(){};
		virtual ~IParameterDescr(){};
		virtual std::string GetDescription(void)=0;
        virtual std::string GetName(void)=0;
	};

class IOutputDescr
    {
    public:
		IOutputDescr(){};
		virtual ~IOutputDescr(){};
		virtual std::string GetDescription(void)=0;
        virtual std::string GetName(void)=0;
    };

template<typename T>
unsigned int linear_index_recursion_func(T it)
    {
    return 0;
    }

template<typename ... Rest> 
unsigned int linear_index_recursion_func(std::list<unsigned int>::const_iterator& it,unsigned int First,Rest... r)
    {
    return First+(*it)*linear_index_recursion_func(++it,r...);
    }

template<typename ... Nums> 
unsigned int linear_index(const std::list<unsigned int>& a,Nums... n )
    {
    unsigned int res=0;
    std::list<unsigned int>::const_iterator dimens_it=a.begin(); 
    res=linear_index_recursion_func(dimens_it,n...);
    return res;
    }

class IOutputData
    {
    public:
        IOutputData(){};
        virtual ~IOutputData(){};
        virtual std::string GetName(void)=0;
        virtual unsigned int GetCount(void)=0;
        virtual const std::list<unsigned int>& GetDimensions(void)=0;
        virtual std::vector<double>& GetData(void)=0;
    };


class ParameterBase:public IParameter
    {
    private:
        std::string name;
        double* param;
    public:
        ParameterBase(const std::string&,double* );
        virtual ~ParameterBase(){};
        virtual void SetValue(const double &);
        virtual double GetValue(void);
		virtual std::string GetName(void);
    };

class ParameterDescrBase:public IParameterDescr
    {
    private:
        std::string name;
        std::string descr;
    public:
        ParameterDescrBase(const std::string&,const std::string&);
        virtual ~ParameterDescrBase(){};
        virtual std::string GetDescription(void);
        virtual std::string GetName(void);
    };

class OutputDescrBase:public IOutputDescr
    {
    private:
        std::string name;
        std::string descr;
    public:
        OutputDescrBase(const std::string&,const std::string&);
        virtual ~OutputDescrBase(){};
        virtual std::string GetDescription(void);
        virtual std::string GetName(void);
    };

template<class T>
class BaseContainer
    {
    public:
            
        class iterator
            {
            private:
                friend BaseContainer;
                
                typename std::map<std::string,std::shared_ptr<T> >::iterator it;
                
                iterator(const typename  std::map<std::string,std::shared_ptr<T> >::iterator & InIt):
                                it(InIt)
                    {
                    }
            
            
            public:
                iterator()
                    {
                    }
                iterator(const iterator& in):it(in.it)
                    {
                    }
                ~iterator()
                    {
                    }
                operator std::shared_ptr<T>()
                    {
                    return it->second;
                    }
                iterator& operator=(const iterator& in)
                    {
                    it=in.it;
                    return *this;
                    }
                bool operator==(const iterator& in) const
                    {
                    return it==in.it;
                    }
                bool operator!=(const iterator& in) const
                    {
                    return it!=in.it;
                    }
                iterator& operator++()
                    {
                    ++it;
                    return *this;
                    }
                iterator& operator--()
                    {
                    --it;
                    return *this;
                    }
                T& operator*() const
                    {
                    return *(it->second);
                    }
                std::shared_ptr<T> operator->() const
                    {
                    return it->second;
                    }
            };
    
    private:
    
        std::map<std::string,std::shared_ptr<T> > ItemMap;
    
    protected:
    
        void AddItem(const std::string& na,const std::shared_ptr<T> &in)
            {
            if(ItemMap.find(na)==ItemMap.end())
                ItemMap.insert(
                    std::pair<std::string,std::shared_ptr<T> >(na,in));
            else
                {
                std::cout<<na<<std::endl;
                throw 1;
                }
            }
        
    public:
        BaseContainer(){};
        virtual ~BaseContainer(){};
        
        T& operator[](const std::string& na)
            {
            auto it=ItemMap.find(na);
            if(it==ItemMap.end())
                throw std::invalid_argument("There aro no "+na+" element in container");
            return *(it->second);
            }
    
        iterator begin()
            {
            return BaseContainer::iterator(ItemMap.begin());
            }
        iterator end()
            {
            return BaseContainer::iterator(ItemMap.end());
            }
    
        long unsigned int size(void)
            {
            return ItemMap.size();
            }
    };

class ICalculator
    {
    public:
        ICalculator(){};
        virtual ~ICalculator(){};
        virtual BaseContainer<IParameter> & params(void)=0;
        virtual BaseContainer<IOutputData> & calculate(void)=0;
    };

class ICalculatorFabric
    {
    public:
        ICalculatorFabric(){};
        virtual ~ICalculatorFabric(){};
        virtual const std::string& get_task_description(void)=0;
        virtual BaseContainer<IParameterDescr> & get_parametr_descr(void)=0;
        virtual BaseContainer<IOutputDescr> & get_output_descr(void)=0;
        virtual std::shared_ptr<ICalculator> get_calculator_instance(void)=0;
    };

template<typename T>
class elist
	{
		class inserter
			{
				elist* parent;
			public:
				inserter(elist* ptr):parent(ptr){};
				inserter(const inserter& in){parent=in.parent;};
				operator elist(){return *parent;}
				inserter& operator()(const T& in)
					{
					parent->list_inst.push_back(in);
					return *this;
					}
			};
		
		std::list<T> list_inst;
		
		
	public:
		elist()
			{
			
			}
		~elist(){};
		typename std::list<T>::iterator begin(void)
			{
			return list_inst.begin();
			}
		typename std::list<T>::iterator end(void)
			{
			return list_inst.end();
			}
		inserter push_back(const T& in)
			{
			list_inst.push_back(in);
			return inserter(this);
			}
	};

class task_description;

class task_description_holder
    {
    private:
        friend task_description;
        task_description_holder(){};
        task_description_holder(const task_description_holder&){};
        task_description_holder& operator=(const task_description_holder&){return *this;};
    
        std::map<std::string,std::string> descrs;        
        
    public:
         static task_description_holder& Instance()
            {
            static task_description_holder theSingleInstance;
            return theSingleInstance;
            }
    
        class item_puter
            {
            public:
                item_puter(const std::string&,const std::string&);
            };
    };

class task_description
    {
        std::map<std::string,std::string>::iterator it;
    public:
        task_description(const std::string&);
        const std::string& to_string();
    };

#define make_description(first , ...) task_description_holder::item_puter BOOST_PP_CAT(str,__LINE__)(std::string(#first),std::string(#__VA_ARGS__));\
        __VA_ARGS__

}

#endif /*__CompManag_base_hpp_*/
