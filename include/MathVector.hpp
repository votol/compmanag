#ifndef __MathVector_h_
#define __MathVector_h_
//#include <stdio.h>

template<typename T>
class MathVector
    {
        unsigned int num_elements;
        T *massive;
    public: 
        MathVector()
            {
            num_elements=0;
            massive=0;
            }
        MathVector(unsigned int n)
            {
            num_elements=n;
            massive=new T[num_elements];
            }
        MathVector(unsigned int n,const T & element)
            {
            num_elements=n;
            massive=new T[num_elements];
            for(int k=0;k<n;k++)
                {
                massive[k]=element;
                }
            }
        MathVector(const MathVector & in)
            {
            num_elements=in.num_elements;
            massive=new T[num_elements];
            for(unsigned int i=0;i<num_elements;i++)
                {
                massive[i]=in.massive[i];
                }
            }
        ~MathVector()
            {
            if(massive!=0)
                delete [] massive;
            }
        void resize(unsigned int n)
            {
            if(n>num_elements)
                {
                T* massive_per=new T[n];
                for(unsigned int i=0;i<num_elements;i++)
                    {
                    massive_per[i]=massive[i];
                    }
                if(massive!=0)    
                    delete [] massive;
                massive=massive_per;
                num_elements=n;
                }
            else if(n<num_elements)
                {
                T* massive_per=new T[n];
                for(unsigned int i=0;i<n;i++)
                    {
                    massive_per[i]=massive[i];
                    }
                if(massive!=0)    
                    delete [] massive;
                massive=massive_per;
                num_elements=n;
                }
            }
        void resize(unsigned int n,const T & element)
            {
            if(n>num_elements)
                {
                T* massive_per=new T[n];
                for(unsigned int i=0;i<num_elements;i++)
                    {
                    massive_per[i]=massive[i];
                    }
                for(unsigned int i=num_elements;i<n;i++)
                    {
                    massive_per[i]=element;
                    }
                if(massive!=0)    
                    delete [] massive;
                massive=massive_per;
                num_elements=n;
                }
            else if(n<num_elements)
                {
                T* massive_per=new T[n];
                for(unsigned int i=0;i<n;i++)
                    {
                    massive_per[i]=massive[i];
                    }
                if(massive!=0)    
                    delete [] massive;
                massive=massive_per;
                num_elements=n;
                }
            }
        T* get_data(void)
            {
            return massive;
            }
        T & operator [](unsigned int n)
            {
            if(n<num_elements)
                return massive[n];
            else
                throw 1;
            }
        const MathVector & operator =(const MathVector &in)
            {
            if(massive!=0)
                delete [] massive;
            num_elements=in.num_elements;
            massive=new T[num_elements];
            for(unsigned int i=0;i<num_elements;i++)
                {
                massive[i]=in.massive[i];
                }
            return *this;
            }
        MathVector operator +(const MathVector &in)
            {
            if(num_elements>in.num_elements)    
                {
                MathVector per(num_elements);
                for(unsigned int i=0;i<in.num_elements;i++)
                    {
                    per.massive[i]=massive[i]+in.massive[i];
                    }
                for(unsigned int i=in.num_elements;i<num_elements;i++)
                    {
                    per.massive[i]=massive[i];
                    }
                return per;
                }
            else
                {
                MathVector per(in.num_elements);
                for(unsigned int i=0;i<num_elements;i++)
                    {
                    per.massive[i]=massive[i]+in.massive[i];
                    }
                for(unsigned int i=num_elements;i<in.num_elements;i++)
                    {
                    per.massive[i]=in.massive[i];
                    }
                return per;
                }
            }
        MathVector operator -(const MathVector &in)
            {
            if(num_elements>in.num_elements)    
                {
                MathVector per(num_elements);
                for(unsigned int i=0;i<in.num_elements;i++)
                    {
                    per.massive[i]=massive[i]-in.massive[i];
                    }
                for(unsigned int i=in.num_elements;i<num_elements;i++)
                    {
                    per.massive[i]=massive[i];
                    }
                return per;
                }
            else
                {
                MathVector per(in.num_elements);
                for(unsigned int i=0;i<num_elements;i++)
                    {
                    per.massive[i]=massive[i]-in.massive[i];
                    }
                for(unsigned int i=num_elements;i<in.num_elements;i++)
                    {
                    per.massive[i]=-in.massive[i];
                    }
                return per;
                }
            }
        MathVector & operator +=(const MathVector &in)
            {
            for(unsigned int i=0;i<((num_elements<in.num_elements)?num_elements:in.num_elements);i++)
                {
                massive[i]+=in.massive[i];
                }
            return *this;
            }
        MathVector & operator -=(const MathVector &in)
            {
            for(unsigned int i=0;i<((num_elements<in.num_elements)?num_elements:in.num_elements);i++)
                {
                massive[i]-=in.massive[i];
                }
            return *this;
            }
        MathVector operator *(const T & in)
            {
            MathVector per(*this);
            for(unsigned int i=0;i<num_elements;i++)
                {
                per.massive[i]*=in;
                }
            return per;
            }
        MathVector operator /(const T & in)
            {
            MathVector per(*this);
            for(unsigned int i=0;i<num_elements;i++)
                {
                per.massive[i]/=in;
                }
            return per;
            }
        MathVector & operator *=(const T & in)
            {
            for(unsigned int i=0;i<num_elements;i++)
                {
                massive[i]*=in;
                }
            return *this;
            }
        MathVector & operator /=(const T & in)
            {
            for(unsigned int i=0;i<num_elements;i++)
                {
                massive[i]/=in;
                }
            return *this;
            }
    };


template<typename T>
MathVector<T> operator *(const T& a,const MathVector<T> &b)
  {
  return b*a;
  }

#endif
