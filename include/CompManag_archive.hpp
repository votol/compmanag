#ifndef __CompManag_archive_hpp_
#define __CompManag_archive_hpp_
#include "CompManag_base.hpp"
#include <thread>
#include <mutex>
#include <chrono>
#include <list>
#include <map>
#include <stdexcept>
#include <fstream>
#include "boost/filesystem.hpp"

namespace CompManag
{
namespace archive
{
#define UUID "1f27ef71f2454a1381ac1ae74a631889"
#define UUID_SIZE 32





class IDataRecord
    {
    public:
        IDataRecord(){};
        virtual ~IDataRecord(){};
        virtual std::string& GetShortDescription()=0;
        virtual std::string& GetFullDescription()=0;
        virtual std::string& GetLabel()=0;
        virtual BaseContainer<IParameter>& GetParameters()=0;
        virtual BaseContainer<IOutputData>& GetOutputs()=0;
    };

class DataRecordVoid:public IDataRecord
	{
		BaseContainer<IParameter> param;
		BaseContainer<IOutputData> data;
		std::string str;
	public:
		DataRecordVoid(){};
        virtual ~DataRecordVoid(){};
        virtual std::string& GetShortDescription();
        virtual std::string& GetFullDescription();
		virtual std::string& GetLabel();
		virtual BaseContainer<IParameter>& GetParameters();
		virtual BaseContainer<IOutputData>& GetOutputs();
	};



class IDataWriter
    {
    public:
        IDataWriter(){};
        virtual ~IDataWriter(){};
        virtual void AddRecord(const std::string& label,BaseContainer<IParameter> & params, BaseContainer<IOutputData> & data)=0;
    };

template<typename T>
class base_iterator
    {
        std::shared_ptr<base_iterator> ptr;
    public:
        base_iterator(){};
        base_iterator(const base_iterator& in){ptr=in.copy();};
        virtual ~base_iterator(){};
        
        virtual base_iterator& operator=(const base_iterator& in)
			{
			ptr=in.copy();
            return *this;
			}
        virtual bool operator==(const base_iterator& in)
			{
			if(in.ptr)
			    return (*ptr)==(*(in.ptr));
			else
			    return (*ptr)==in;
			}
        virtual bool operator!=(const base_iterator& in)
			{
            if(in.ptr)
                return (*ptr)!=(*(in.ptr));
            else
                return (*ptr)!=in;
			}
        virtual T& operator*()
			{
			return *(*(ptr));
			}
        virtual T* operator->()
			{
			return &(*(*ptr));
			}
        virtual base_iterator& operator++()
			{
			++(*ptr);
            return *this;
			}
        virtual base_iterator& operator--()
			{
			--(*ptr);
            return *this;
			}
		virtual std::shared_ptr<base_iterator> copy(void)const
			{
			return ptr->copy();
			}
    };

class IDataContainer
    {
    public:
        IDataContainer(){};
        virtual ~IDataContainer(){};
        virtual std::string& GetShortDescription()=0;
        virtual std::string& GetFullDescription()=0;
        virtual base_iterator<IDataRecord> begin()=0;
        virtual base_iterator<IDataRecord> end()=0;
        virtual long unsigned int size()=0;
    };

class IDataContainerLabel:public IDataContainer
    {
    public:
        IDataContainerLabel(){};
        virtual ~IDataContainerLabel(){};
        virtual IDataContainer& operator[](const std::string& in)=0;
    };

class BaseDataRecord:public IDataRecord
	{
	private:
		class Parameter:public IParameter 
			{
				std::string par_name;
				double par_value;
			public:
				Parameter(const std::string name, const double & value);
				~Parameter(){};
				virtual void SetValue(const double &);
				virtual double GetValue(void);
				virtual std::string GetName(void);
			};
		
		class OutputData:public IOutputData
			{
				std::vector<double> data_data;
				std::list<unsigned int> dimensions;
				std::string data_name;
			public:
				OutputData(IOutputData& data);
				~OutputData(){};
				virtual std::string GetName(void);
				virtual unsigned int GetCount(void);
				virtual const std::list<unsigned int>& GetDimensions(void);
				virtual std::vector<double>& GetData(void);
			};
		
		class ParameterContainer:public BaseContainer<IParameter>
			{
			public:
				ParameterContainer(BaseContainer<IParameter> & params);
				virtual ~ParameterContainer(){};
			};
		class OutputDataContainer:public BaseContainer<IOutputData>
			{
			public:
				OutputDataContainer(BaseContainer<IOutputData> & data);
				OutputDataContainer(BaseContainer<IOutputData> & data,elist<std::string>& data_names);
				virtual ~OutputDataContainer(){};
			};
		std::string void_string;
		std::string BaseDataRecord_label;
		ParameterContainer ParameterContainerInstance;
		OutputDataContainer OutputDataContainerInstance;
	public:
		BaseDataRecord(const std::string& label,BaseContainer<IParameter> & params, BaseContainer<IOutputData> & data);
		BaseDataRecord(const std::string& label,BaseContainer<IParameter> & params, BaseContainer<IOutputData> & data,elist<std::string>&& data_names);
		virtual ~BaseDataRecord(){};
		virtual std::string& GetShortDescription();
		virtual std::string& GetFullDescription();
		virtual std::string& GetLabel();
		virtual BaseContainer<IParameter>& GetParameters();
		virtual BaseContainer<IOutputData>& GetOutputs();
	};


class archiver_v1
    {
    private:
        class archiver_v1_DataRecord:public IDataRecord
            {
            private:
                class Parameter:public IParameter 
                    {
                        std::string par_name;
                        double par_value;
                    public:
                        Parameter(const std::string name, const double & value);
                        ~Parameter(){};
                        virtual void SetValue(const double &);
                        virtual double GetValue(void);
                        virtual std::string GetName(void);
                    };
                
                class OutputData:public IOutputData
                    {
                        std::vector<double> data_data;
                        std::list<unsigned int> dimensions;
                        std::string data_name;
                    public:
                        OutputData(IOutputData& data);
                        OutputData(const std::string& name,std::istream& ist);
                        ~OutputData(){};
                        virtual std::string GetName(void);
                        virtual unsigned int GetCount(void);
                        virtual const std::list<unsigned int>& GetDimensions(void);
                        virtual std::vector<double>& GetData(void);
                    };
                
                class ParameterContainer:public BaseContainer<IParameter>
                    {
                    public:
                        ParameterContainer(BaseContainer<IParameter> & params);
                        ParameterContainer(std::istream& ist);
                        virtual ~ParameterContainer(){};
                    };
                class OutputDataContainer:public BaseContainer<IOutputData>
                    {
                    public:
                        OutputDataContainer(BaseContainer<IOutputData> & data);
                        OutputDataContainer(std::istream& ist);
                        virtual ~OutputDataContainer(){};
                    };
                archiver_v1* parent;
                std::string archiver_v1_DataRecord_label;
                ParameterContainer ParameterContainerInstance;
                OutputDataContainer OutputDataContainerInstance;
            public:
                archiver_v1_DataRecord(archiver_v1* ptr,const std::string& label,BaseContainer<IParameter> & params, BaseContainer<IOutputData> & data);
                archiver_v1_DataRecord(archiver_v1* ptr,const std::string& label,std::istream& ist);
                ~archiver_v1_DataRecord(){};
                virtual std::string& GetShortDescription();
                virtual std::string& GetFullDescription();
                virtual std::string& GetLabel();
                virtual BaseContainer<IParameter>& GetParameters();
                virtual BaseContainer<IOutputData>& GetOutputs();
            };
        
        class archiver_v1_DataContainerLabel:public IDataContainerLabel
            {
            private:
                class iterator:public base_iterator<IDataRecord>
                    {
                        std::list<archiver_v1_DataRecord>::iterator it;
                    public:
                        iterator(const std::list<archiver_v1_DataRecord>::iterator& in);
                        ~iterator(){};
                        iterator(const iterator& in);
                        base_iterator<IDataRecord>& operator=(const base_iterator<IDataRecord>& in);
                        bool operator==(const base_iterator<IDataRecord>& in);
                        bool operator!=(const base_iterator<IDataRecord>& in);
                        IDataRecord& operator*();
                        IDataRecord* operator->();
                        base_iterator<IDataRecord>& operator++();
                        base_iterator<IDataRecord>& operator--();
                        std::shared_ptr<base_iterator<IDataRecord> > copy(void)const;
                    };
                
                class DataContainer:public IDataContainer
                    {
                        class iterator:public base_iterator<IDataRecord>
                            {
                                std::list<std::list<archiver_v1_DataRecord>::iterator>::iterator it;
                            public:
                                iterator(const std::list<std::list<archiver_v1_DataRecord>::iterator>::iterator& in);
                                ~iterator(){};
                                iterator(const iterator& in);
                                base_iterator<IDataRecord>& operator=(const base_iterator<IDataRecord>& in);
                                bool operator==(const base_iterator<IDataRecord>& in);
                                bool operator!=(const base_iterator<IDataRecord>& in);
                                IDataRecord& operator*();
                                IDataRecord* operator->();
                                base_iterator<IDataRecord>& operator++();
                                base_iterator<IDataRecord>& operator--();
                                std::shared_ptr<base_iterator<IDataRecord> > copy(void)const;
                            };
                        archiver_v1* parent;
                        std::list<std::list<archiver_v1_DataRecord>::iterator> data_iterators;
                    public:
                        DataContainer(archiver_v1* ptr);
                        ~DataContainer(){};
                        virtual std::string& GetShortDescription();
                        virtual std::string& GetFullDescription();
                        virtual base_iterator<IDataRecord> begin();
                        virtual base_iterator<IDataRecord> end();
                        virtual long unsigned int size();
                        void push_back(const std::list<archiver_v1_DataRecord>::iterator& in);
                    };
                
                class label_not_exists:public std::invalid_argument
                    {
                        std::string name;
                    public:
                        label_not_exists(const std::string& in);
                        virtual ~label_not_exists();
                        virtual const char* what() const noexcept;
                    };
                
                archiver_v1* parent;
                std::list<archiver_v1_DataRecord> archiver_v1_DataContainerLabel_data;
                std::map<std::string, DataContainer> archiver_v1_DataContainerLabel_data_ptrs;
            public:
                archiver_v1_DataContainerLabel(archiver_v1* ptr);
                virtual ~archiver_v1_DataContainerLabel(){};
                virtual std::string& GetShortDescription();
                virtual std::string& GetFullDescription();
                virtual base_iterator<IDataRecord> begin();
                virtual base_iterator<IDataRecord> end();
                virtual long unsigned int size();
                virtual IDataContainer& operator[](const std::string& in);
                std::list<archiver_v1::archiver_v1_DataRecord>::iterator add_data(const std::string& label,BaseContainer<IParameter> & params, BaseContainer<IOutputData> & data);
				void add_data(std::istream& ist);
            };
        class archiver_v1_DataWriter:public IDataWriter
            {
                archiver_v1* parent;
            public:
                archiver_v1_DataWriter(archiver_v1* ptr);
                virtual void AddRecord(const std::string& label,BaseContainer<IParameter> & params, BaseContainer<IOutputData> & data);
            };
        
        void read_description_from_file(std::istream& ist);
        void write_to_file_record(archiver_v1_DataRecord& rec);
        void file_writing_function();
        
        
        
        std::string archiver_file_name;
        std::string task_short_description;
        std::string task_full_description;
        archiver_v1_DataContainerLabel* archiver_v1_DataContainerLabel_instance;
        std::mutex archiver_v1_DataContainerLabel_instance_mutex; 
		
        bool is_working;
        std::mutex is_working_mutex;
        std::thread saving_to_file_thread;
        std::list<std::list<archiver_v1_DataRecord>::iterator> list_of_records_for_writing;
        std::mutex list_of_records_for_writing_mutex;
        
        
    protected:
        archiver_v1(void){archiver_v1_DataContainerLabel_instance=0;};
    public:
        archiver_v1(const std::string& file_name);
        archiver_v1(const std::string& file_name,const std::string & short_descr,const std::string & full_descr);
        virtual ~archiver_v1();
        virtual IDataContainerLabel& GetData();
        virtual std::shared_ptr<IDataWriter> GetWriterInstance();
    };

}
}

#endif /*__CompManag_archive_hpp_*/
