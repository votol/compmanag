#ifndef __CompManag_runge_hpp_
#define __CompManag_runge_hpp_

#include <functional>
#include "CompManag_base.hpp"
#include "MathVector.hpp"

namespace CompManag
{
namespace Runge
{
template<typename DATA>
using SolverInit = std::function<void(DATA * /*coordinate massive to init*/)>;

template<typename DATA>
using TimeDerivative = std::function<void(const DATA */*input coords*/,DATA * /*derivative result*/,const double/*current time*/)>;

template<typename DATA>
using SolverOutputMaker = std::function<bool(const DATA */*current coords*/,const double/*current time*/)>;

template<typename DATA>
struct RungeSolverInit
    {
    double TimeStep;
    unsigned int NumTimeSteps;
    unsigned int OutputNumTimeSteps;
    unsigned int Dimension;
    SolverInit<DATA> Init;
    TimeDerivative<DATA> Derivative;
    SolverOutputMaker<DATA> OutputMaker;
    };

template<typename DATA>
class RungeSolver
    {
    public:
        
    private:
        unsigned int CurrentStep;
        MathVector<DATA> teck;
        MathVector<DATA> per_vector[4];
        double Time;
        RungeSolverInit<DATA> my_params;
        RungeSolver(const RungeSolver&){};
    public:
        RungeSolver(){};
        ~RungeSolver(){};
        void Init(const RungeSolverInit<DATA> & in)
            {
            my_params=in;
            teck.resize(my_params.Dimension);
            per_vector[0].resize(my_params.Dimension);
            per_vector[1].resize(my_params.Dimension);
            per_vector[2].resize(my_params.Dimension);
            per_vector[3].resize(my_params.Dimension);
            }
        void Calculate(void)
            {
            unsigned int count1,count2;
            Time=0.0;
            my_params.Init(teck.get_data());
            count2=0;
            for(count1=0;count1<my_params.NumTimeSteps;++count1)
                {
                if(count2==my_params.OutputNumTimeSteps)
                    {
                    if(!my_params.OutputMaker(teck.get_data(),Time))
                        return;
                    count2=0;
                    }
                my_params.Derivative(teck.get_data(),per_vector[0].get_data(),Time);
                per_vector[1]=per_vector[0];
                per_vector[1]/=DATA(6.0);
                per_vector[3]=per_vector[0];
                per_vector[3]*=DATA(0.5*my_params.TimeStep);
                per_vector[2]=teck;
                per_vector[2]+=per_vector[3];
                my_params.Derivative(per_vector[2].get_data(),per_vector[0].get_data(),Time+0.5*my_params.TimeStep);
                per_vector[3]=per_vector[0];
                per_vector[3]/=DATA(3.0);
                per_vector[1]+=per_vector[3];
                per_vector[3]=per_vector[0];
                per_vector[3]*=DATA(0.5*my_params.TimeStep);
                per_vector[2]=teck;
                per_vector[2]+=per_vector[3];
                my_params.Derivative(per_vector[2].get_data(),per_vector[0].get_data(),Time+0.5*my_params.TimeStep);
                per_vector[3]=per_vector[0];
                per_vector[3]/=DATA(3.0);
                per_vector[1]+=per_vector[3];
                per_vector[3]=per_vector[0];
                per_vector[3]*=DATA(my_params.TimeStep);
                per_vector[2]=teck;
                per_vector[2]+=per_vector[3];
                my_params.Derivative(per_vector[2].get_data(),per_vector[0].get_data(),Time+my_params.TimeStep);
                per_vector[3]=per_vector[0];
                per_vector[3]/=DATA(6.0);
                per_vector[1]+=per_vector[3];
                per_vector[1]*=DATA(my_params.TimeStep);
                teck+=per_vector[1];
                Time+=my_params.TimeStep;
                count2++;
                }
            }
    };

}
}


#endif /*__CompManag_runge_hpp_*/
