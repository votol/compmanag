#ifndef __CompManag_cmd_hpp
#define __CompManag_cmd_hpp_
#include <functional>
#include <list>
#include "CompManag_base.hpp"

namespace CompManag
{
using CmdCommand = std::function<void(const std::list<std::string>&)>;


class cmd_processor
    {
        class IVariable
            {
            public:
                IVariable(){};
                virtual ~IVariable(){};
                virtual void SetValue(const std::string&)=0;
                virtual void PrintValue()=0;
            };
        
        class IntVariable:public IVariable
            {
                int* my_ptr;
            public:
                IntVariable(int* ptr);
                virtual ~IntVariable(){};
                virtual void SetValue(const std::string& in);
                virtual void PrintValue();
            };
        
        class DoubleVariable:public IVariable
            {
                double* my_ptr;
            public:
                DoubleVariable(double* ptr);
                virtual ~DoubleVariable(){};
                virtual void SetValue(const std::string& in);
                virtual void PrintValue();
            };
        
        class IParameterVariable:public IVariable
            {
                std::shared_ptr<IParameter> my_ptr;
            public:
                IParameterVariable(std::shared_ptr<IParameter> ptr);
                virtual ~IParameterVariable(){};
                virtual void SetValue(const std::string& in);
                virtual void PrintValue();
            };
        
        std::map<std::string,std::shared_ptr<IVariable> > variables;
        std::map<std::string,CmdCommand > commands;
    
    
        bool IsFinished;
    
        void print(const std::list<std::string>& params);
        void exit(const std::list<std::string>& params);
        void help(const std::list<std::string>& params);
        
        void variable_processor(std::string& name,std::string& val);
        void function_processor(std::string& name,std::string& val);
        void cmd_main();
    public:
        cmd_processor();
        ~cmd_processor();
        void add_variable(const std::string& name,const std::string type,void* ptr);
        void add_variable(BaseContainer<IParameter>& in);
        void add_command(const std::string name,const CmdCommand& com);
        void run();
        void arun();
    };

}

#endif /*__CompManag_cmd_hpp_*/
